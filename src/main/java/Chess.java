import java.util.Scanner;

public class Chess {


    public static final String BLACK = "\u25A1";
    public static final String WHITE = "\u220E";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Specify the size of the Chesseboard: ");

        int chesse_size = scanner.nextInt();
        System.out.println("Chesse size: " + chesse_size + "\n");
        System.out.println("You draw Chessboard of lenght: " + chesse_size);

        for (int i = 0; i < chesse_size; i++) {
            for (int j = 0; j < chesse_size; j++) {

                if ((i + j) % 2 == 0) {
                    System.out.print(BLACK+" ");
                } else {
                    System.out.print(WHITE+" ");
                }
            }
            System.out.println("");
        }
    }
}
